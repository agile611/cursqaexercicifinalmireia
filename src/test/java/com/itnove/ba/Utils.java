package com.itnove.ba;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertTrue;

public class Utils {

    // mètode acceptar alerta
    public void AlertaAcceptar(WebDriverWait wait, WebDriver driver) {
        try {
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            alert.accept();
        } catch (Exception e) {
            //exception handling
        }
    }


    // mètode per clicar
    public void clicar (WebElement e) throws InterruptedException {
        e.click();
        Thread.sleep(3000);
    }

    // mètode per clicar tres vegades
    public void clicar3 (WebElement e1, WebElement e2, WebElement e3 ) throws InterruptedException {
        e1.click();
        Thread.sleep(3000);
        e2.click();
        Thread.sleep(3000);
        e3.click();
        Thread.sleep(3000);
    }

    //mètode per desplegar un menú i anar clicant cada item
    public void desplegableClic (WebDriver driver, List<WebElement> llista,
                                 String xpath1, String xpath2) throws InterruptedException {
        for (int i = 0; i < llista.size(); i++) {
            WebElement item = driver.findElement(By.xpath(xpath1 + (i + 1) + xpath2));
            item.click();
            Thread.sleep(3000);
        }
    }


    // mètode per desplegar un menú i escollir una opció
    public void desplegableEscollirOpcio (WebDriver driver, List<WebElement> llista,
                                          String xpathItems1, String xpathItems2, String s) throws InterruptedException {

            for (int i = 0; i < llista.size(); i++) {
                WebElement item = driver.findElement(By.xpath(xpathItems1 + (i + 1) + xpathItems2));
                if (item.getText().equalsIgnoreCase(s)) {
                    item.click();
                }
                Thread.sleep(3000);
            }
    }


    // mètode per escriure en un quadre de text
    public void escriure (WebElement e, String miss) throws InterruptedException {
        e.click();
        e.clear();
        e.sendKeys(miss);
        Thread.sleep(3000);
    }

    // mètode per escriure en un quadre de text i clicar: cercar..
    public void escriureIClicar (WebElement e, String miss, WebElement e2) throws InterruptedException {
        e.click();
        e.clear();
        e.sendKeys(miss);
        e2.click();
        Thread.sleep(3000);
    }


    //mètode estàndard per introduir login
    public void logar (WebDriver driver, String url, WebElement user, String userParaula,
                       WebElement pass, String passParaula, WebElement boto) throws InterruptedException {
        navegar(driver,url);
        escriure(user, userParaula);
        escriure(pass, passParaula);
        clicar(boto);
    }


    // mètode per navegar fins a una pàgina
    public void navegar (WebDriver driver, String url) throws InterruptedException {
        driver.get(url);
        Thread.sleep(3000);
    }





}

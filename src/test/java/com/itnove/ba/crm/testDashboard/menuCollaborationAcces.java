package com.itnove.ba.crm.testDashboard;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;


public class menuCollaborationAcces extends BaseTest {

    @Test
    public void accesPaginesMenu () throws InterruptedException {

        //accedir al dashboard
        dL.logarCRM(driver,urlCrm);

        //clicar sobre menú a desplegar
        u.clicar(dD.menuCollaborationSup);

        //accedir a cada pàgina a partir de cada item del menú, comprovant que canvia de url a cada iteració
        dD.accesItems(driver, dD.llistaCollaborationSup,dD.xpathCollaboration1,dD.xpathCollaboration2,dD.menuCollaborationSup);


    }

}

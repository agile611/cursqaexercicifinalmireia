package com.itnove.ba.crm.testDashboard;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;


public class menuAllAcces extends BaseTest {

    @Test
    public void accesPaginesMenu () throws InterruptedException {

        //accedir al dashboard
        dL.logarCRM(driver,urlCrm);

        //clicar sobre menú a desplegar
        u.clicar(dD.menuAllSup);

        //accedir a cada pàgina a partir de cada item del menú, comprovant que canvia de url a cada iteració
        dD.accesItems(driver, dD.llistaAllSup,dD.xpathAll1,dD.xpathAll2,dD.menuAllSup);


    }

}

package com.itnove.ba.crm.testDashboard;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class CercaCorrecte extends BaseTest {


    @Test
    public void cercarParaula () throws InterruptedException {

        //accedir al dashboard
        dL.logarCRM(driver,urlCrm);

        //clicar icona per fer la cerca
        u.clicar(dD.botoLupaSup);

        //Fer la cerca
        u.escriureIClicar(dD.quadreCercaSup,dD.paraulaCercar,dD.botoLupaDinsCerca);
        assertTrue(dD.referenciaCercaCorrecte.getAttribute("value").contains(dD.paraulaCercar));

    }

}

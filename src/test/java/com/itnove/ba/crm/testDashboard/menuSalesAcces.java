package com.itnove.ba.crm.testDashboard;

import com.itnove.ba.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class menuSalesAcces extends BaseTest {

    @Test
    public void accesPaginesMenu () throws InterruptedException {

        //accedir al dashboard
        dL.logarCRM(driver,urlCrm);

        //clicar sobre menú a desplegar
        u.clicar(dD.menuSalesSup);

        //accedir a cada pàgina a partir de cada item del menú, comprovant que canvia de url a cada iteració
        dD.accesItems(driver, dD.llistaSalesSup,dD.xpathSales1,dD.xpathSales2,dD.menuSalesSup);


    }

}

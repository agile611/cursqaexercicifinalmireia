package com.itnove.ba.crm.testDashboard;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;


public class menuActivitiesAcces extends BaseTest {

    @Test
    public void accesPaginesMenu () throws InterruptedException {

        //accedir al dashboard
        dL.logarCRM(driver,urlCrm);

        //clicar sobre menú a desplegar
        u.clicar(dD.menuActivitiesSup);

        //accedir a cada pàgina a partir de cada item del menú, comprovant que canvia de url a cada iteració
        dD.accesItems(driver, dD.llistaActivitiesSup,dD.xpathActivities1,dD.xpathActivities2,dD.menuActivitiesSup);


    }

}

package com.itnove.ba.crm.testDashboard;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;


public class menuSupportAcces extends BaseTest {

    @Test
    public void accesPaginesMenu () throws InterruptedException {

        //accedir al dashboard
        dL.logarCRM(driver,urlCrm);

        //clicar sobre menú a desplegar
        u.clicar(dD.menuSupportSup);

        //accedir a cada pàgina a partir de cada item del menú, comprovant que canvia de url a cada iteració
        dD.accesItems(driver, dD.llistaSupportSup,dD.xpathSupport1,dD.xpathSupport2,dD.menuSupportSup);


    }

}

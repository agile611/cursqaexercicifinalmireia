package com.itnove.ba.crm.testLogin;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class LogarIncorrecteUserBuit extends BaseTest {


    @Test
    public void logarUserBuit () throws InterruptedException {

        u.logar(driver, urlCrm,dL.quadreUser," ",dL.quadrePassword,dL.passwordKo,dL.botoLogin);
        assertTrue(dL.missatgeErrorLogin.isDisplayed());

    }
}

package com.itnove.ba.crm.testLogin;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class LogarIncorrecteUserPasswordBuits extends BaseTest {


    @Test
    public void logarUserBuitPasswordBuit () throws InterruptedException {

        u.logar(driver, urlCrm,dL.quadreUser," ",dL.quadrePassword," ",dL.botoLogin);
        assertTrue(dL.missatgeErrorLogin.isDisplayed());

    }
}

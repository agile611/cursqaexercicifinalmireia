package com.itnove.ba.crm.testLogin;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class LogarIncorrecteUser extends BaseTest {


    @Test
    public void logarUserKo () throws InterruptedException {

        u.logar(driver, urlCrm,dL.quadreUser,dL.userKo,dL.quadrePassword,dL.passwordKo,dL.botoLogin);
        assertTrue(dL.missatgeErrorLogin.isDisplayed());

    }
}

package com.itnove.ba.crm.testLogin;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class LogOut extends BaseTest {

    @Test
    public void logoutOk () throws InterruptedException {

        dL.logarCRM(driver, urlCrm);
        u.clicar(dL.botoNinot);
        u.desplegableEscollirOpcio(driver,dL.llistaNinot,dL.xpathNinot1,dL.xpathNinot2,dL.itemLogout);
        assertTrue(dL.botoLogin.isDisplayed());
    }
}

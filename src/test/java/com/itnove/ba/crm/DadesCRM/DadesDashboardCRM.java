package com.itnove.ba.crm.DadesCRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static org.testng.Assert.assertTrue;

public class DadesDashboardCRM {

    private WebDriver driver2;


    // url dashboard
    public String urlCrmDashboard ="http://crm.votarem.lu/index.php?module=Home&action=index";

    //Dades accés als ítems del menú superior esquerre
    @FindBy(xpath = ".//*[@id='toolbar']/ul[1]/li[1]/a[1]")
    public WebElement iconHome;

        //menu sales
    @FindBy(xpath = ".//*[@id='grouptab_0']")
    public WebElement menuSalesSup;

    @FindBy(xpath = "//*[@id=\"toolbar\"]/ul[1]/li[2]/span[2]/ul[1]/li")
    public List<WebElement> llistaSalesSup;

    public String xpathSales1 = "//*[@id=\"toolbar\"]/ul[1]/li[2]/span[2]/ul[1]/li[";
    public String xpathSales2 = "]/a";

        //menu marketing
    @FindBy(xpath = ".//*[@id='grouptab_1']")
    public WebElement menuMarketingSup;

    @FindBy(xpath = "//*[@id=\"toolbar\"]/ul[1]/li[3]/span[2]/ul[1]/li")
    public List<WebElement> llistaMarketingSup;

    public String xpathMarketing1 = "//*[@id=\"toolbar\"]/ul[1]/li[3]/span[2]/ul[1]/li[";
    public String xpathMarketing2 = "]/a";

        //menu support
    @FindBy(xpath = ".//*[@id='grouptab_2']")
    public WebElement menuSupportSup;

    @FindBy(xpath = "//*[@id=\"toolbar\"]/ul[1]/li[4]/span[2]/ul[1]/li")
    public List<WebElement> llistaSupportSup;

    public String xpathSupport1 = "//*[@id=\"toolbar\"]/ul[1]/li[4]/span[2]/ul[1]/li[";
    public String xpathSupport2 = "]/a";

        //menu Activities
    @FindBy(xpath = ".//*[@id='grouptab_3']")
    public WebElement menuActivitiesSup;

    @FindBy(xpath = "//*[@id=\"toolbar\"]/ul[1]/li[5]/span[2]/ul[1]/li")
    public List<WebElement> llistaActivitiesSup;

    public String xpathActivities1 = "//*[@id=\"toolbar\"]/ul[1]/li[5]/span[2]/ul[1]/li[";
    public String xpathActivities2 = "]/a";

        //menu Collaboration
    @FindBy(xpath = ".//*[@id='grouptab_4']")
    public WebElement menuCollaborationSup;

    @FindBy(xpath = "//*[@id=\"toolbar\"]/ul[1]/li[6]/span[2]/ul[1]/li")
    public List<WebElement> llistaCollaborationSup;

    public String xpathCollaboration1 = "//*[@id=\"toolbar\"]/ul[1]/li[6]/span[2]/ul[1]/li[";
    public String xpathCollaboration2 = "]/a";

        //menu All
    @FindBy(xpath = ".//*[@id='grouptab_5']")
    public WebElement menuAllSup;

    @FindBy(xpath = "//*[@id=\"toolbar\"]/ul[1]/li[7]/span[2]/ul[1]/li")
    public List<WebElement> llistaAllSup;

    public String xpathAll1 = "//*[@id=\"toolbar\"]/ul[1]/li[7]/span[2]/ul[1]/li[";
    public String xpathAll2 = "]/a";


    //DADES PER A LA CERCA
    public String paraulaCercar= "Administrator";

    @FindBy(xpath = "//*[@id=\"query_string\"]")
    public WebElement quadreCercaSup;

    @FindBy(xpath = "//*[@id=\"searchbutton\"]")
    public WebElement botoLupaSup;

    @FindBy(xpath = "//*[@id=\"searchformdropdown\"]/div/span/button")
    public WebElement botoLupaDinsCerca;

    @FindBy(xpath = "//*[@id=\"searchFieldMain\"]")
    public WebElement referenciaCercaCorrecte;



    //mètode per accedir als diferents ítems de cada menú superior esquerre del Dashboard
    public void accesItems (WebDriver driver, List<WebElement> llista, String xpath1,
                            String xpath2, WebElement menu) throws InterruptedException {
        for (int i = 0; i < llista.size(); i++) {
            WebElement item = driver.findElement(By.xpath(xpath1 + (i + 1) +xpath2));
            item.click();
            Thread.sleep(5000);
            assertTrue(!driver.getCurrentUrl().equalsIgnoreCase(urlCrmDashboard));
            iconHome.click();
            menu.click();
            Thread.sleep(5000);
        }
    }


    public DadesDashboardCRM(WebDriver driver) {
        PageFactory.initElements(driver, this);
        driver2 = driver;
    }
}

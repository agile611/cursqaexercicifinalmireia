package com.itnove.ba.crm.DadesCRM;

import com.itnove.ba.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class DadesLoginCRM {

    private WebDriver driver2;

    // Dades a usar
    public String userOk = "user";
    public String passwordOk = "bitnami";

    public String userKo ="hola";
    public String passwordKo = "adeu";

    public String email ="prova@gmail.com";
    public int numUsuaris = 1000;

    //elements amb xpath
    @FindBy(xpath = ".//*[@id='user_name']")
    public WebElement quadreUser;

    @FindBy(xpath = ".//*[@id='user_password']")
    public WebElement quadrePassword;

    @FindBy(xpath = ".//*[@id='bigbutton']")
    public WebElement botoLogin;

    @FindBy(xpath = "(//*[@id='usermenucollapsed'])[2]")
    public WebElement botoNinot;

    @FindBy(xpath = "//*[@id=\"globalLinks\"]/ul/li")
    public List<WebElement> llistaNinot;

    public String xpathNinot1 ="//*[@id=\"globalLinks\"]/ul/li[";
    public String xpathNinot2 ="]/a";
    public String itemLogout = "Log Out";


    //dades per usar el recordatori de contrasenya
    @FindBy(xpath = "//*[@id=\"forgotpasslink\"]/a")
    public WebElement forgottenPassword;

    @FindBy(xpath = "//*[@id='fp_user_name']")
    public WebElement quadreForgotUser;

    @FindBy(xpath = "//*[@id='fp_user_mail']")
    public WebElement quadreEmail;

    @FindBy(xpath = "//*[@id='generate_pwd_button']")
    public WebElement botoSubmit;

    @FindBy(xpath = "//*[id='generate_success']")
    public WebElement missatgeErrorEmail;

    @FindBy(xpath = "/html/body/div[1]/div[2]/p")
    public WebElement missatgeErrorLogin;

    //mètode per logar-se correctament
    public void logarCRM (WebDriver driver, String url) throws InterruptedException {
        Utils u = new Utils();
        u.logar(driver, url,quadreUser,userOk,quadrePassword,passwordOk,botoLogin);
    }

    public DadesLoginCRM(WebDriver driver) {
        PageFactory.initElements(driver, this);
        driver2 = driver;
    }

}

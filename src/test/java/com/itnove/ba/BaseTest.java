package com.itnove.ba;



import com.itnove.ba.crm.DadesCRM.DadesDashboardCRM;
import com.itnove.ba.crm.DadesCRM.DadesLoginCRM;
import com.itnove.ba.opencart.Dades.DadesAdminGestio;
import com.itnove.ba.opencart.Dades.DadesAdminLogin;
import com.itnove.ba.opencart.Dades.DadesBotigaCerca;
import com.itnove.ba.opencart.Dades.DadesBotigaCheckout;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileBrowserType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;


/**
 * Created by guillem on 29/02/16.
 */
public class BaseTest {

    //DADES GENERAL
    public RemoteWebDriver driver;
    public WebDriverWait wait;
    public int timeOut = 10;
    public Utils u;


    //DADES OPENCART
    public String urlOpencartBotiga = "http://opencart.votarem.lu/index.php?route=common/home";
    public String urlOpencartAdmin = "http://opencart.votarem.lu/admin/index.php?route=common/login";
    public DadesAdminGestio dAG;
    public DadesAdminLogin dAL;
    public DadesBotigaCerca dBC;
    public DadesBotigaCheckout dBCh;

    //DADES CRM
    public String urlCrm = "http://crm.votarem.lu/index.php?action=Login&module=Users";
    public DadesLoginCRM dL;
    public DadesDashboardCRM dD;

    @Parameters({"site"})
    @BeforeClass
    public void setUp() throws IOException {

       /* Opció a verificar
        String site= System.getProperty("site");
        try {
            if (site != null && site.equalsIgnoreCase("firefox")) {
                DesiredCapabilities capabilities = DesiredCapabilities.firefox();
                System.setProperty("webdriver.gecko.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "geckodriver.exe");
                driver = new FirefoxDriver(capabilities);
            } else if (site != null && site.equalsIgnoreCase("chrome")) {
                DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                System.setProperty("webdriver.chrome.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver.exe");
                driver = new ChromeDriver(capabilities);
            } else if (site != null && site.equalsIgnoreCase("android")) {
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability("deviceName", "Android Emulator");
                capabilities.setCapability("platformVersion", "5.1");
                capabilities.setCapability("browserName", MobileBrowserType.BROWSER);
                driver = new AndroidDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), capabilities);
            } else if (site != null && site.equalsIgnoreCase("iphone")) {
                DesiredCapabilities caps = DesiredCapabilities.iphone();
                caps.setCapability("appiumVersion", "1.7.1");
                caps.setCapability("deviceName", "iPhone 6 Plus Simulator");
                caps.setCapability("deviceOrientation", "portrait");
                caps.setCapability("platformVersion", "10.0");
                caps.setCapability("platformName", "iOS");
                caps.setCapability("browserName", "Safari");
                driver = new IOSDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
            }
        }catch (WebDriverException e){ //Per assegurar que funcionen els test, ja que fins ara els hem executat per aquesta via
            System.out.println(e.getMessage());
        }
        */

       //mentre no es verifiqui l'anterior opció, farem els test en el navegador chrome
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        System.setProperty("webdriver.chrome.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver.exe");
        driver = new ChromeDriver(capabilities);


        //DADES GENERALS
        wait = new WebDriverWait(driver, timeOut);
        u = new Utils();
        driver.manage().window().fullscreen();

        //DADES OPENCART
        dAG = new DadesAdminGestio(driver);
        dAL = new DadesAdminLogin(driver);
        dBC = new DadesBotigaCerca(driver);
        dBCh = new DadesBotigaCheckout(driver);

        //DADES CRM
        dL = new DadesLoginCRM(driver);
        dD = new DadesDashboardCRM(driver);

    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

}

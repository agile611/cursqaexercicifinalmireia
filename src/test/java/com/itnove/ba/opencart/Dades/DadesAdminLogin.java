package com.itnove.ba.opencart.Dades;

import com.itnove.ba.BaseTest;
import com.itnove.ba.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DadesAdminLogin extends BaseTest {

    private WebDriver driver2;



    // Dades a usar
    public String userOk = "user";
    public String passwordOk = "bitnami1";

    public String userKo ="hola";
    public String passwordKo = "adeu";

    public String email ="prova@gmail.com";
    public int numUsuaris = 1000;

    //dades xpath
    @FindBy(xpath = "id(\"input-username\")")
    public WebElement quadreUser;

    @FindBy(xpath = "id(\"input-password\")")
    public WebElement quadrePassword;

    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/button[1]")
    public WebElement botoLogin;

    //missatge d'error quan el login és incorrecte
    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/i")
    public WebElement missatgeErrorLogin;

    //per tancar missatge emergent al entrar al dashboard
    @FindBy(xpath = "id(\"modal-security\")/div[1]/div[1]/div[1]/button[1]")
    public WebElement botoTancarMissEmergent;

    @FindBy(xpath = "id(\"header\")/div[1]/ul[1]/li[2]/a[1]")
    public WebElement botoLogout;


    //dades per usar el recordatori de contrasenya
    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/span[1]/a[1]")
    public WebElement forgottenPassword;

    @FindBy(xpath = "id(\"input-email\")")
    public WebElement quadreEmail;

    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/button[1]")
    public WebElement botoReset;

    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/a[1]")
    public WebElement botoCancel;

    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/i")
    public WebElement missatgeErrorEmail;

    public void logarOP (WebDriver driver, String url) throws InterruptedException {
        Utils u = new Utils();
        u.logar(driver, url,quadreUser,userOk,quadrePassword,passwordOk,botoLogin);
    }


    public DadesAdminLogin(WebDriver driver) {
        PageFactory.initElements(driver, this);
        driver2 = driver;
    }


}

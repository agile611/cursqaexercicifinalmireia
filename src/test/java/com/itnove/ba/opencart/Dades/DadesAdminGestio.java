package com.itnove.ba.opencart.Dades;

import com.itnove.ba.BaseTest;
import com.itnove.ba.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class DadesAdminGestio extends BaseTest {

    private WebDriver driver2;

    public String urlDashboard = "http://opencart.votarem.lu/admin/index.php?route=common/dashboard&user_token=mDG3y3uO9NwglX8wjhlfnvHA8RhzIEeL";

    // dades pel menú de Catalog
    @FindBy(xpath = "id(\"menu-catalog\")")
    public WebElement menuCatalog;

    @FindBy(xpath = "id(\"collapse1\")/li")
    public List<WebElement> llistaCatalog;

    public String xpathItemsCatalog1 = "id(\"collapse1\")/li[";
    public String xpathItemsCatalog2 = "]/a[1]";

    //element a comparar que es troba a totes les pàgines
    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/h1[1]")
    public WebElement nomItem;

    //dades per cada item del desplegable menuCatalog
    //item categoria
    @FindBy(xpath = "id(\"collapse1\")/li[1]/a[1]")
    public WebElement quadreCategories;

    public String categoriaNom = "zzzCategoria";
    public String categoriaNomKo = " ";
    public String categoriaTag = "novaTagCategoria";
    public String categoriaDescripcio = "Això és una prova";

    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/a[1]")
    public WebElement botoCategoriaAdd;

    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/button[1]")
    public WebElement botoCategoriaDelete;

    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/button")// és el mateix que el botoDelete
    public WebElement botoCategoriaGrabar;

    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/div[1]/a[1]") //és el mateix que el botoAdd
    public WebElement botoCategoriaCancel;

    @FindBy(xpath = "id(\"form-category\")/ul[1]/li[1]/a[1]")
    public WebElement formulariCategoriaGeneral;

    @FindBy(xpath = "id(\"language\")/li[1]/a[1]")
    public WebElement referenciaCategoriaGeneral;

    @FindBy(xpath = "id(\"form-category\")/ul[1]/li[2]/a[1]")
    public WebElement formulariCategoriaData;

    @FindBy(xpath = "id(\"form-category\")/ul[1]/li[3]/a[1]")
    public WebElement formulariCategoriaSEO;

    @FindBy(xpath = "id(\"form-category\")/ul[1]/li[4]/a[1]")
    public WebElement formulariCategoriaDesign;

    @FindBy(xpath = "id(\"input-name1\")")
    public WebElement quadreCategoriaNom;

    @FindBy(xpath = "id(\"language1\")/div[2]/div[1]/div[1]/div[3]/div[2]")
    public WebElement quadreCategoriaDescripcio;

    @FindBy(xpath = "id(\"input-meta-title1\")")
    public WebElement quadreCategoriaTag;

    @FindBy(xpath = "id(\"content\")/div[2]/div[1]/i")
    public WebElement missatgeCategoriesModificades;

    @FindBy(xpath = "//*[@id=\"content\"]/div[2]/div[1]/i[@class='fa fa-check-circle']")
    public WebElement missatgeCategoriesCorrecte;

    @FindBy(xpath = "//*[@id=\"content\"]/div[2]/div[1]/i[@class='fa fa-exclamation-circle']")
    public WebElement missatgeCategoriesError;

    @FindBy(xpath = "id(\"form-category\")/div[1]/table[1]/tbody[1]/tr")
    public List<WebElement> llistaCategories;

    public String xpathItemsCategories1 = "id(\"form-category\")/div[1]/table[1]/tbody[1]/tr[";
    public String xpathItemsCategories2 = "]/td[2]";

    public String xpathItemsCheckboxCategories2 = "]/td[1]";

    @FindBy(xpath = " .//ul[@class='pagination']/li[last()-1]/a") //escull el botó penúltim per passar les pàgines e//
    public WebElement fletxaEndavantCategories;


    //item products
    @FindBy(xpath = "id(\"collapse1\")/li[2]/a[1]")
    public WebElement quadreProducts;

    //item RecurringProfiles
    @FindBy(xpath = "id(\"collapse1\")/li[3]/a[1]")
    public WebElement quadreRecurringProfiles;


    //item Filters
    @FindBy(xpath = "id(\"collapse1\")/li[4]/a[1]")
    public WebElement quadreFilters;

    //item Attributes
    @FindBy(xpath = "id(\"collapse1\")/li[5]/a[1]")
    public WebElement quadreAttributes;

    @FindBy(xpath = "id(\"collapse5\")/li[1]/a[1]")
    public WebElement quadreAttributesSubAttributes;

    @FindBy(xpath = "id(\"collapse5\")/li[2]/a[1]")
    public WebElement quadreAttributesSubGroup;

    // item Options
    @FindBy(xpath = "id(\"collapse1\")/li[6]/a[1]")
    public WebElement quadreOptions;


    // item Manufactures
    @FindBy(xpath = "id(\"collapse1\")/li[7]/a[1]")
    public WebElement quadreManufactures;

    // item Downloads
    @FindBy(xpath = "id(\"collapse1\")/li[8]/a[1]")
    public WebElement quadreDownloads;


    // item Reviews
    @FindBy(xpath = "id(\"collapse1\")/li[9]/a[1]")
    public WebElement quadreReviews;


    // item Information
    @FindBy(xpath = "id(\"collapse1\")/li[11]/a[1]")
    public WebElement quadreInformation;




    public DadesAdminGestio(WebDriver driver) {
        PageFactory.initElements(driver, this);
        driver2 = driver;
    }
}

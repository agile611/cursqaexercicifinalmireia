package com.itnove.ba.opencart.Dades;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class DadesBotigaCerca {


    private WebDriver driver2;


    //productes a cercar
    public String [] productes = {"mac","monitors","windows"};
    public String producteIncorrecte= "ABCD";


    // Dades per a la cerca
    @FindBy (xpath = "id(\"search\")/span[1]/button[1]")
    public WebElement botoLupa;

    @FindBy (xpath = "id(\"search\")/input[1]")
    public WebElement quadreSupCerca;

    @FindBy (xpath = "//*[@id=\"content\"]/h2")
    public WebElement referenciaFiltreCerca;

    @FindBy (xpath = "//*[@id=\"content\"]/p[2]")
    public WebElement referenciaCercaBuida;

    @FindBy (xpath = "//*[@id=\"input-search\"]")
    public WebElement quadreFiltreCerca;

    @FindBy (xpath = "//div[2]/div/div/p/label/input[@id=(\"description\")]")
    public WebElement checkBoxFiltreCerca;

    @FindBy (xpath = "//*[@class=(\"checkbox-inline\")]/input")
    public WebElement checkBoxFiltreSubCategories;

    @FindBy (xpath = "id(\"button-search\")")
    public WebElement botoFiltreCerca;

    @FindBy (xpath = "id(\"content\")/div[1]/div[2]")
    public WebElement menuFiltreCategories;

    @FindBy (xpath = "id(\"content\")/div[1]/div[2]/select[1]/option")
    public List<WebElement> llistaFiltreCategories;

    //per buscar categoria usarem aquest xpath + i
    public String xpathSubCategories1 = "id(\"content\")/div[1]/div[2]/select[1]/option[";
    public String xpathSubCategories2 ="]";
    public String subcategoria = "Desktops";

    //posar visió o mode graella un cop feta la busqueda
    @FindBy (xpath = "id(\"grid-view\")")
    public WebElement botoResultCercaGraella;

    //prèmer add de producte
    @FindBy (xpath = "//*[@id=\"content\"]/div[3]")
    public WebElement marcarLlistaProducteAdd;

    @FindBy (xpath = "//*[@id=\"content\"]/div[3]/div")
    public List<WebElement> llistaAddProductes;

    public String xpath1AddProducte = "//*[@id='content']/div[3]/div[";
    public String xpath2AddProducte = "]/div/div[2]/div[2]/button[1]";



     public DadesBotigaCerca(WebDriver driver) {
        PageFactory.initElements(driver, this);
        driver2 = driver;
    }

}

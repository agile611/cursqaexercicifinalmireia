package com.itnove.ba.opencart.Dades;

import com.itnove.ba.BaseTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class DadesBotigaCheckout extends BaseTest {

    private WebDriver driver3;


    //Dades a omplir pel checkout
    public String nom = "mireia";
    public String cognom = "s";
    public String telef = "654654654";
    public String email = "prova@gmail.com";
    public String password = "opencartprova";
    public String adress = "carrer mallorca";
    public String ciutat = "Barcelona";
    public String cp = "08032";
    public String pais ="Spain";
    public String regio ="Barcelona";

    //Dades pel checkout
    @FindBy(xpath = "id(\"top-links\")/ul[1]/li[5]")
    public WebElement botoCheckOutSup;

    @FindBy (xpath = "//*[@id=\"content\"]/h1")
    public WebElement referenciaCheckoutPag;

    @FindBy (xpath = "//*[@id=\"cart\"]/ul/li[2]/div/p/a[2]")
    public WebElement botoCheckOutquadreCarret;

    @FindBy (xpath = "id(\"cart\")/button[1]")
    public WebElement botoCarretquadre;

    @FindBy(xpath = "//*[@id=\"product-search\"]/div[1]/i[@class='fa fa-check-circle']")
    public WebElement missatgeConfirmarAdd;


    // checkboxs del 'new customer'
    @FindBy (xpath = "id(\"collapse-checkout-option\")/div[1]/div[1]/div[1]/div[1]/label[1]")
    public WebElement checkboxRegisterAccount;

    @FindBy (xpath = "id(\"collapse-checkout-option\")/div[1]/div[1]/div[1]/div[2]/label[1]")
    public WebElement checkboxGuestCheckout;

    @FindBy (xpath = "id(\"button-account\")")
    public WebElement botoContinue1;


    //dades segon pas
    @FindBy (xpath = " id(\"input-payment-firstname\")")
    public WebElement quadreFirstName;

    @FindBy (xpath = "id(\"input-payment-lastname\")")
    public WebElement quadreLasttName;

    @FindBy (xpath = "id(\"input-payment-email\")")
    public WebElement quadreEmail;

    @FindBy (xpath = "id(\"input-payment-telephone\")")
    public WebElement quadreTelephone;

    @FindBy (xpath = "id(\"input-payment-password\")")
    public WebElement quadrePassword;

    @FindBy (xpath = "id(\"input-payment-confirm\")")
    public WebElement quadrePasswordConfirm;

    @FindBy (xpath = "id(\"input-payment-address-1\")")
    public WebElement quadreAdress1;

    @FindBy (xpath = "id(\"input-payment-city\")")
    public WebElement quadreCity;

    @FindBy (xpath = "id(\"input-payment-postcode\")")
    public WebElement quadrePostCode;

    @FindBy (xpath = "//*[@id=\"input-payment-country\"]")
    public WebElement desplegablePais;

    @FindBy (xpath = "//*[@id=\"input-payment-country\"]/option")
    public List<WebElement> llistaPais;

    public String xpathPais1 = "//*[@id='input-payment-country']/option[";
    public String xpathPais2 = "]";

    @FindBy (xpath = "id(\"input-payment-zone\")")
    public WebElement desplegableRegio;

    @FindBy (xpath = "id(\"input-payment-zone\")/option")
    public List<WebElement> llistaRegio;

    public String xpathRegion1 = "//*[@id=\"input-payment-zone\"]/option[";
    public String xpathRegion2 = "]";

    @FindBy (xpath = "id(\"collapse-payment-address\")/div[1]/div[3]/label[1]")
    public WebElement checkboxAdressesSame;

    @FindBy (xpath = "id(\"collapse-payment-address\")/div[1]/div[4]/div[1]/input[1]")
    public WebElement checkboxPrivacyPolicy;

    @FindBy (xpath = "id(\"button-register\")")
    public WebElement botoContinue2;

    // dades pel 3er pas
    @FindBy (xpath = "//*[@id=\"collapse-shipping-address\"]/div/form/div[1]/label")
    public WebElement checkboxUseAdress;

    @FindBy (xpath = "id(\"shipping-existing\")/select[1]")
    public List<WebElement> llistaUseAdress;

    @FindBy (xpath = "id(\"collapse-shipping-address\")/div[1]/form[1]/div[3]/label[1]")
    public WebElement checkboxNewAdress;

    @FindBy (xpath = "id(\"button-shipping-address\")")
    public WebElement botoContinue3;

    //dades pel 4rt pas
    @FindBy (xpath = "id(\"collapse-shipping-method\")/div[1]/div[1]/label[1]")
    public WebElement checkboxRecarrecEnv;

    @FindBy (xpath = "id(\"button-shipping-method\")")
    public WebElement botoContinue4;

    //dades pel 5é pas
    @FindBy (xpath = "id(\"collapse-payment-method\")/div[1]/div[1]/label[1]")
    public WebElement checkboxContrareembolsament;

    @FindBy (xpath = "id(\"collapse-payment-method\")/div[1]/div[2]/div[1]/input[1]")
    public WebElement checkboxTermesCondicionts;

    @FindBy (xpath = "id(\"button-payment-method\")")
    public WebElement botoContinue5;


    //dades pel 6é pas
    @FindBy (xpath = "id(\"button-confirm\")")
    public WebElement botoConfirmacio;

    // canvi pàgina quan acaba procés
    @FindBy (xpath = "//*[@id=\"content\"]/h1")
    public WebElement referenciaCheckoutAcabat;



    public DadesBotigaCheckout(WebDriver driver) {
        PageFactory.initElements(driver, this);
        driver3 = driver;
    }

}

package com.itnove.ba.opencart.testAdmin.testLogin;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class LogarCorrecte extends BaseTest {

    @Test
    public void logarOk () throws InterruptedException {

        dAL.logarOP(driver, urlOpencartAdmin);
        u.clicar(dAL.botoTancarMissEmergent);
        assertTrue(!driver.getCurrentUrl().equalsIgnoreCase(urlOpencartAdmin));


    }




}

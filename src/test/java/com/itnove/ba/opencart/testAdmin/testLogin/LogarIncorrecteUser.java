package com.itnove.ba.opencart.testAdmin.testLogin;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;


import static org.testng.Assert.*;



public class LogarIncorrecteUser extends BaseTest {


    @Test
    public void logarUserKo () throws InterruptedException {

        u.logar(driver, urlOpencartAdmin,dAL.quadreUser,dAL.userKo,dAL.quadrePassword,dAL.passwordOk,dAL.botoLogin);
        assertTrue(driver.getCurrentUrl().contains(dAL.missatgeErrorLogin.getText()));

    }
}

package com.itnove.ba.opencart.testAdmin.testGestioEstocs;

import com.itnove.ba.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class AccesACadaPaginaMenu extends BaseTest {

    @Test
    public void accesPaginesMenu () throws InterruptedException {

        //accedir al dashboard d'Admin
        dAL.logarOP(driver, urlOpencartAdmin);
        u.clicar(dAL.botoTancarMissEmergent);
        assertTrue(!driver.getCurrentUrl().equalsIgnoreCase(urlOpencartAdmin));

        //clicar sobre menú a desplegar
        u.clicar(dAG.menuCatalog);

        //accedir a cada pàgina a partir de cada item del menú
        for (int i=0;i<dAG.llistaCatalog.size();i++){
            WebElement item = driver.findElement(By.xpath(dAG.xpathItemsCatalog1+(i+1)+dAG.xpathItemsCatalog2));
            u.clicar(item);
            assertTrue(!driver.getCurrentUrl().equalsIgnoreCase(dAG.urlDashboard));
            if (item.equals(dAG.quadreAttributes)){
                u.clicar(dAG.quadreAttributesSubAttributes);
                assertTrue(!driver.getCurrentUrl().equalsIgnoreCase(dAG.urlDashboard));
                u.clicar(dAG.quadreAttributesSubGroup);
                assertTrue(!driver.getCurrentUrl().equalsIgnoreCase(dAG.urlDashboard));
            }

        }

    }

}

package com.itnove.ba.opencart.testAdmin.testLogin;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class LogarIncorrecteUserBuit extends BaseTest {


    @Test
    public void logarUserBuit () throws InterruptedException {

        u.logar(driver, urlOpencartAdmin,dAL.quadreUser," ",dAL.quadrePassword,dAL.passwordKo,dAL.botoLogin);
        assertTrue(driver.getCurrentUrl().contains(dAL.missatgeErrorLogin.getText()));

    }
}

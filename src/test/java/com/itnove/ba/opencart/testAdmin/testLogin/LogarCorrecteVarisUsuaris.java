package com.itnove.ba.opencart.testAdmin.testLogin;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class LogarCorrecteVarisUsuaris extends BaseTest {

    @Test
    public void logarOkVarisUsuaris () throws InterruptedException {

        for (int i=0; i<dAL.numUsuaris;i++) {
            dAL.logarOP(driver, urlOpencartAdmin);
            u.clicar(dAL.botoTancarMissEmergent);
            assertTrue(!driver.getCurrentUrl().equalsIgnoreCase(urlOpencartAdmin));
            u.clicar(dAL.botoLogout);
        }


    }




}

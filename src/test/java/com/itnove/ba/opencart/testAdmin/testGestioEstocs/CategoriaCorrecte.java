package com.itnove.ba.opencart.testAdmin.testGestioEstocs;

import com.itnove.ba.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CategoriaCorrecte extends BaseTest {

    @Test
    public void afegirCategoria () throws InterruptedException {
;
       //entrar dashboard
        dAL.logarOP(driver, urlOpencartAdmin);
        u.clicar3(dAL.botoTancarMissEmergent,dAG.menuCatalog,dAG.quadreCategories);

        //afegir categoria
        u.clicar(dAG.botoCategoriaAdd);
        u.clicar(dAG.formulariCategoriaGeneral);
        u.escriure(dAG.quadreCategoriaNom,dAG.categoriaNom);
        u.escriure(dAG.quadreCategoriaDescripcio,dAG.categoriaDescripcio);
        u.escriure(dAG.quadreCategoriaTag,dAG.categoriaTag);
        assertTrue(dAG.referenciaCategoriaGeneral.isDisplayed());

        u.clicar3(dAG.formulariCategoriaData,dAG.formulariCategoriaSEO,dAG.formulariCategoriaDesign);

        //grabar la categoria
        u.clicar(dAG.botoCategoriaGrabar);
        assertTrue(driver.getCurrentUrl().contains(dAG.missatgeCategoriesModificades.getText()));
        assertTrue(driver.getCurrentUrl().contains(dAG.missatgeCategoriesCorrecte.getText()));

    }

    @Test (dependsOnMethods = { "afegirCategoria" })
    public void eliminarCategoria () throws InterruptedException {

        boolean trobat=false;

         // per buscar la categoria creada
        do {
            for (int i = 0; i < dAG.llistaCategories.size(); i++) {
                WebElement item = driver.findElement(By.xpath(dAG.xpathItemsCategories1 + (i + 1) + dAG.xpathItemsCategories2));
                WebElement checkboxItem = driver.findElement(By.xpath(dAG.xpathItemsCategories1 + (i + 1) +
                        dAG.xpathItemsCheckboxCategories2));
                if (item.getText().equalsIgnoreCase(dAG.categoriaNom)) {
                    checkboxItem.click();
                    trobat =true;
                    //eliminar la categoria
                    u.clicar(dAG.botoCategoriaDelete);
                    u.AlertaAcceptar(wait, driver);
                    assertTrue(driver.getCurrentUrl().contains(dAG.missatgeCategoriesModificades.getText()));
                }
                else {
                    u.clicar(dAG.fletxaEndavantCategories);
                }
            }
       }while (!trobat);
    }
}

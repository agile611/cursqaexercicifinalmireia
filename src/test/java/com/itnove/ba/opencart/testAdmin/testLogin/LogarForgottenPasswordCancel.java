package com.itnove.ba.opencart.testAdmin.testLogin;

import com.itnove.ba.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class LogarForgottenPasswordCancel extends BaseTest {

    @Test
    public void logarContrasenyaOblidada () throws InterruptedException {

        u.clicar(dAL.forgottenPassword);
        assertTrue(!driver.getCurrentUrl().equalsIgnoreCase(urlOpencartAdmin));
        u.clicar(dAL.botoCancel);
        Assert.assertTrue(driver.getCurrentUrl().equalsIgnoreCase(urlOpencartAdmin));


    }




}

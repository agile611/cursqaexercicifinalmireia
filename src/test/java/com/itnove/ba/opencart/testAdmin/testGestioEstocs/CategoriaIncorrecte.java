package com.itnove.ba.opencart.testAdmin.testGestioEstocs;

import com.itnove.ba.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CategoriaIncorrecte extends BaseTest {

    @Test
    public void afegirCategoriaBuida () throws InterruptedException {
;
       //entrar dashboard
        dAL.logarOP(driver, urlOpencartAdmin);
        u.clicar3(dAL.botoTancarMissEmergent,dAG.menuCatalog,dAG.quadreCategories);

        //afegir categoria
        u.clicar(dAG.botoCategoriaAdd);
        u.clicar(dAG.formulariCategoriaGeneral);
        u.escriure(dAG.quadreCategoriaNom,dAG.categoriaNomKo); //deixem el camp del nom de la categoria buit
        u.escriure(dAG.quadreCategoriaDescripcio,dAG.categoriaDescripcio);
        u.escriure(dAG.quadreCategoriaTag,dAG.categoriaTag);
        assertTrue(dAG.referenciaCategoriaGeneral.isDisplayed());

        u.clicar3(dAG.formulariCategoriaData,dAG.formulariCategoriaSEO,dAG.formulariCategoriaDesign);

        //intentar grabar la categoria
        u.clicar(dAG.botoCategoriaGrabar);
        assertTrue(driver.getCurrentUrl().contains(dAG.missatgeCategoriesModificades.getText()));
        //assertTrue(driver.getCurrentUrl().contains(dAG.missatgeCategoriesError.getText())); BUG -- crea la categoria havent
            // un camp obligatori en blanc

        // cancel·lar la creació de la categoria
        u.clicar(dAG.botoCategoriaCancel);

    }


}

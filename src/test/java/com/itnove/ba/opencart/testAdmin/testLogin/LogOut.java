package com.itnove.ba.opencart.testAdmin.testLogin;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class LogOut extends BaseTest {

    @Test
    public void logoutOk () throws InterruptedException {

        dAL.logarOP(driver, urlOpencartAdmin);
        u.clicar(dAL.botoTancarMissEmergent);
        assertTrue(!driver.getCurrentUrl().equalsIgnoreCase(urlOpencartAdmin));
        u.clicar(dAL.botoLogout);
        assertTrue(dAL.botoLogin.isDisplayed());


    }




}

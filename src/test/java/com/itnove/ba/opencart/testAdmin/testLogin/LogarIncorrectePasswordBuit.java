package com.itnove.ba.opencart.testAdmin.testLogin;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class LogarIncorrectePasswordBuit extends BaseTest {


    @Test
    public void logarPasswordBuit () throws InterruptedException {

        u.logar(driver, urlOpencartAdmin,dAL.quadreUser,dAL.userOk,dAL.quadrePassword," ",dAL.botoLogin);
        assertTrue(driver.getCurrentUrl().contains(dAL.missatgeErrorLogin.getText()));

    }
}

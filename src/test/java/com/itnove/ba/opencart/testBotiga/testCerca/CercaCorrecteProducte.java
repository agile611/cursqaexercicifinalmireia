package com.itnove.ba.opencart.testBotiga.testCerca;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class CercaCorrecteProducte extends BaseTest {


    @Test
    public void cercarProducte () throws InterruptedException {

        u.navegar(driver,urlOpencartBotiga);
        u.escriureIClicar(dBC.quadreSupCerca,dBC.productes[0],dBC.botoLupa);
        assertTrue(dBC.referenciaFiltreCerca.isDisplayed());
        assertTrue(dBC.quadreSupCerca.getAttribute("value").contains(dBC.productes[0]));

    }

}

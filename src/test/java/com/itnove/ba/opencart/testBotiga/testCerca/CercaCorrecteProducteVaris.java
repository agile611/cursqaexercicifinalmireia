package com.itnove.ba.opencart.testBotiga.testCerca;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class CercaCorrecteProducteVaris extends BaseTest {


    @Test
    public void cercarProducteVaris () throws InterruptedException {

        u.navegar(driver,urlOpencartBotiga);
        for (int i=0; i<dBC.productes.length;i++) {
            u.escriureIClicar(dBC.quadreSupCerca, dBC.productes[i], dBC.botoLupa);
            assertTrue(dBC.quadreSupCerca.getAttribute("value").contains(dBC.productes[i]));
            assertTrue(dBC.referenciaFiltreCerca.isDisplayed());
        }

    }

}

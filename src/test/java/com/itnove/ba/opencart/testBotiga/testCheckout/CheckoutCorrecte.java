package com.itnove.ba.opencart.testBotiga.testCheckout;

import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class CheckoutCorrecte extends Compra {


    @Test (dependsOnGroups = {"Compra.compra"})
    public void checkoutComplet () throws InterruptedException {

       //desprès de fer la compra, cliquem el botò checkout
        u.clicar(dBCh.botoCheckOutSup);

        // introduim camps obligatoris per fer el checkout a partir de crear account
        // pas 1
        u.clicar(dBCh.checkboxRegisterAccount);
        u.clicar(dBCh.botoContinue1);
        //pas 2
        u.escriure(dBCh.quadreFirstName,dBCh.nom); //nom
        u.escriure(dBCh.quadreLasttName, dBCh.cognom); //cognom
        u.escriure(dBCh.quadreEmail,dBCh.email); //email
        u.escriure(dBCh.quadreTelephone,dBCh.telef); //telef
        u.escriure(dBCh.quadrePassword,dBCh.password); //pass
        u.escriure(dBCh.quadrePasswordConfirm,dBCh.password);// confirmar pass
        assertTrue(dBCh.checkboxAdressesSame.isEnabled()); //confirmar que està habilitat aquest checkbox
        u.escriure(dBCh.quadreAdress1,dBCh.adress);// adreça1
        u.escriure(dBCh.quadreCity,dBCh.ciutat); //ciutat
        u.escriure(dBCh.quadrePostCode,dBCh.cp); //codi postal
            //escollir pais
        u.clicar(dBCh.desplegablePais);
        u.desplegableEscollirOpcio(driver,dBCh.llistaPais,dBCh.xpathPais1,dBCh.xpathPais2,dBCh.pais);
            //escollir regio
        u.clicar(dBCh.desplegableRegio);
        u.desplegableEscollirOpcio(driver, dBCh.llistaRegio,dBCh.xpathRegion1, dBCh.xpathRegion2,dBCh.regio);
        u.clicar(dBCh.checkboxPrivacyPolicy);
        u.clicar(dBCh.botoContinue2);

        //pas 3
        //assertTrue(dBCh.checkboxUseAdress.isEnabled());
        u.clicar(dBCh.botoContinue3);

        //pas 4
        assertTrue(dBCh.checkboxRecarrecEnv.isEnabled());
        u.clicar(dBCh.botoContinue4);

        //pas 5
        assertTrue(dBCh.checkboxContrareembolsament.isEnabled());
        u.clicar(dBCh.checkboxTermesCondicionts);
        u.clicar(dBCh.botoContinue5);

        //pas 6
        u.clicar(dBCh.botoConfirmacio);

        //comprovar que s'ha realitzat
        assertTrue(dBCh.referenciaCheckoutAcabat.isDisplayed());

    }

}

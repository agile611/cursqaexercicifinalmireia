package com.itnove.ba.opencart.testBotiga.testCerca;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class CercaInCorrecteProducte extends BaseTest {


    @Test
    public void cercarProducteInexistent () throws InterruptedException {

        u.navegar(driver,urlOpencartBotiga);
        u.escriureIClicar(dBC.quadreSupCerca,dBC.producteIncorrecte,dBC.botoLupa);
        assertTrue(dBC.referenciaCercaBuida.isDisplayed());

    }

}

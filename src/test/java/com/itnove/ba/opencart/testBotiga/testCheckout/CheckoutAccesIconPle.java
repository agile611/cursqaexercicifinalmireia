package com.itnove.ba.opencart.testBotiga.testCheckout;

import com.itnove.ba.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class CheckoutAccesIconPle extends Compra {

// prèmer icon checkout de la part superior del dashboard amb el carret ple amb MAC's


    @Test (dependsOnGroups = {"Compra.compra"})
    public void checkoutIconSup () throws InterruptedException {

        u.clicar(dBCh.botoCheckOutSup);
        assertTrue(dBCh.referenciaCheckoutPag.isDisplayed());





    }

}

package com.itnove.ba.opencart.testBotiga.testCheckout;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class CheckoutAccesIconBuit extends BaseTest {

// prèmer icon checkout de la part superior del dashboard amb el carret BUIT

    @Test
    public void checkoutIconSup () throws InterruptedException {

        u.navegar(driver,urlOpencartBotiga);
        u.clicar(dBCh.botoCheckOutSup);

        assertTrue(dBC.referenciaCercaBuida.isDisplayed());

    }

}

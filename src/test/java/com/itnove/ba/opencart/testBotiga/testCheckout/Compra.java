package com.itnove.ba.opencart.testBotiga.testCheckout;

import com.itnove.ba.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class Compra extends BaseTest {

    @Test (groups={"Compra.compra"})
    public void compra () throws InterruptedException {

        u.navegar(driver, urlOpencartBotiga);
        u.escriureIClicar(dBC.quadreSupCerca, dBC.productes[0], dBC.botoLupa);
        u.clicar(dBC.botoResultCercaGraella); //mostrar productes en forma de graella
        u.clicar(dBC.marcarLlistaProducteAdd);

        // marcar el botó add de cada un dels productes
        u.desplegableClic(driver,dBC.llistaAddProductes,dBC.xpath1AddProducte,dBC.xpath2AddProducte );
        assertTrue(dBCh.missatgeConfirmarAdd.isDisplayed());

    }
}

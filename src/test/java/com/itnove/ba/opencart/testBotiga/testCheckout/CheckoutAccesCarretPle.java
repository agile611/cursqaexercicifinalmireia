package com.itnove.ba.opencart.testBotiga.testCheckout;

import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class CheckoutAccesCarretPle extends Compra {

// prèmer icon checkout de la part superior del dashboard amb el carret ple amb MAC's


    @Test (dependsOnGroups = {"Compra.compra"})
    public void checkoutQuadreCarretPle () throws InterruptedException {

       //desprès de fer la compra, cliquem el botò checkout dins de la finestretra del carret ple
        u.clicar(dBCh.botoCarretquadre);
        u.clicar(dBCh.botoCheckOutquadreCarret);
        assertTrue(dBCh.referenciaCheckoutPag.isDisplayed());
    }

}

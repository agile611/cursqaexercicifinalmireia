package com.itnove.ba.opencart.testBotiga.testCerca;

import com.itnove.ba.BaseTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class FiltreCercaCompletCorrecte extends BaseTest {

// cercar un producte escollint subcategoria i premem els dos checkboxs tot correcte
    @Test
    public void filtreCerca () throws InterruptedException {

        u.navegar(driver,urlOpencartBotiga);
        u.clicar(dBC.botoLupa);

        u.escriureIClicar(dBC.quadreFiltreCerca,dBC.productes[0],dBC.checkBoxFiltreCerca);
        // clic al menú de categories
        u.clicar(dBC.menuFiltreCategories);

        //busquem dins del desplegable de les subcategories: desktop
       u.desplegableEscollirOpcio(driver,dBC.llistaFiltreCategories,dBC.xpathSubCategories1,
               dBC.xpathSubCategories2,dBC.subcategoria);

        u.clicar(dBC.checkBoxFiltreSubCategories);
        u.clicar(dBC.botoFiltreCerca);

        assertTrue(dBC.botoResultCercaGraella.isDisplayed());

    }

}
